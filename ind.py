'''
Personal pomodoro timer indicator app for GNOME-based DE's.

Requires Python 2.7 or Python 3.4, and GObject Introspection

On Ubuntu:
	apt-get install gobject-introspection
	apt-get install python-gi # or python3-gi
'''
    
import sys
from subprocess import call
from gi.repository import Gtk as gtk
from gi.repository import AppIndicator3 as appindicator
from gi.repository import GLib as glib

APPINDICATOR_ID = 'pomodorotimer'
CYCLES_TIL_LONG_REST = 3
WORK_SECONDS = 3000
REST_SECONDS = 600
LONG_REST_SECONDS = 900
INSTALL_PATH = sys.path[0] # change this to your absolute install path


def save_settings():
	global CYCLES_TIL_LONG_REST, WORK_SECONDS, REST_SECONDS, LONG_REST_SECONDS, INSTALL_PATH
	with open('gpomtimer.conf', 'w') as f:
		f.write(str(CYCLES_TIL_LONG_REST) + '\n')
		f.write(str(int(WORK_SECONDS)) + '\n')
		f.write(str(int(REST_SECONDS)) + '\n')
		f.write(str(int(LONG_REST_SECONDS)))

def load_settings():
	global CYCLES_TIL_LONG_REST, WORK_SECONDS, REST_SECONDS, LONG_REST_SECONDS, INSTALL_PATH
	with open('gpomtimer.conf', 'r') as f:
		CYCLES_TIL_LONG_REST = int(f.readline())
		WORK_SECONDS = int(f.readline())
		REST_SECONDS = int(f.readline())
		LONG_REST_SECONDS = int(f.readline())


class PomodoroTimer(object):

	def __init__(self):
		self.ind = appindicator.Indicator.new(APPINDICATOR_ID, INSTALL_PATH + '/media/work_icon.png', appindicator.IndicatorCategory.APPLICATION_STATUS)
		self.ind.set_status(appindicator.IndicatorStatus.ACTIVE)
		self.seconds = WORK_SECONDS
		self.work_state = True
		self.cycles = 0
		self.status = 'idle'
		self.formatted_set_label(self.seconds)
		self.build_menu()

	def formatted_set_label(self, time):
		minutes = int(time/60)
		seconds = int(time - minutes*60)
		status = ""
		if self.work_state:
			self.ind.set_icon_full(INSTALL_PATH + '/media/work_icon.png', "Work")
		else:
			self.ind.set_icon_full(INSTALL_PATH + '/media/rest_icon.png', "Rest")
		label = "%s:%s" % (str(minutes).zfill(2), str(seconds).zfill(2))
		self.ind.set_label(label, "")

	def build_menu(self):
		self.menu = gtk.Menu()
		item_start = gtk.MenuItem("Start")
		item_stop = gtk.MenuItem("Stop")
		item_reset = gtk.MenuItem("Reset")
		item_options = gtk.MenuItem("Settings")
		item_quit = gtk.MenuItem('Quit')
		item_start.connect('activate', self.start)
		item_stop.connect('activate', self.stop)
		item_reset.connect('activate', self.reset)
		item_options.connect('activate', self.options)
		item_quit.connect('activate', self.quit)
		if self.status == 'idle':
			self.menu.append(item_start)
		elif self.status == 'running':
			self.menu.append(item_stop)
			self.menu.append(item_reset)
		elif self.status == 'paused':
			self.menu.append(item_start)
			self.menu.append(item_reset)
		self.menu.append(item_options)	    
		self.menu.append(item_quit)
		self.menu.show_all()
		self.ind.set_menu(self.menu)

	def start(self, source):
		self.status = 'running'
		self.build_menu()
		self.timer()

	def stop(self, source):
		self.status = 'paused'
		self.build_menu()

	def reset(self, source):
		self.seconds = WORK_SECONDS
		self.cycles = 0
		self.work_state = True
		self.formatted_set_label(self.seconds)
		self.status = 'idle'
		self.build_menu()

	def options(self, source):
		self.options_window = gtk.Window(title="Settings")
		
		hbox = gtk.Box(spacing=6)
		self.options_window.add(hbox)

		listbox = gtk.ListBox()
		listbox.set_selection_mode(gtk.SelectionMode.NONE)
		hbox.pack_start(listbox, True, True, 0)

		row = gtk.ListBoxRow()
		hbox = gtk.Box(orientation=gtk.Orientation.HORIZONTAL, spacing=50)
		row.add(hbox)
		label = gtk.Label("Cycles til long rest", xalign=0)
		adjustment = gtk.Adjustment(0, 0, 100, 1, 10, 0)
		self.cycles_spinbutton = gtk.SpinButton()
		self.cycles_spinbutton.set_adjustment(adjustment)
		hbox.pack_start(label, True, True, 0)
		hbox.pack_start(self.cycles_spinbutton, False, True, 0)
		self.cycles_spinbutton.set_numeric(True)
		self.cycles_spinbutton.set_value(CYCLES_TIL_LONG_REST)

		listbox.add(row)

		row = gtk.ListBoxRow()
		hbox = gtk.Box(orientation=gtk.Orientation.HORIZONTAL, spacing=50)
		row.add(hbox)
		label = gtk.Label("Work period duration", xalign=0)
		adjustment = gtk.Adjustment(0, 0, 100, 1, 10, 0)
		self.work_spinbutton = gtk.SpinButton()
		self.work_spinbutton.set_adjustment(adjustment)
		hbox.pack_start(label, True, True, 0)
		hbox.pack_start(self.work_spinbutton, False, True, 0)
		self.work_spinbutton.set_update_policy(gtk.SpinButtonUpdatePolicy.IF_VALID)
		self.work_spinbutton.set_digits(1)
		self.work_spinbutton.set_value(WORK_SECONDS / 60.0)

		listbox.add(row)

		row = gtk.ListBoxRow()
		hbox = gtk.Box(orientation=gtk.Orientation.HORIZONTAL, spacing=50)
		row.add(hbox)
		label = gtk.Label("Rest period duration", xalign=0)
		adjustment = gtk.Adjustment(0, 0, 100, 1, 10, 0)
		self.rest_spinbutton = gtk.SpinButton()
		self.rest_spinbutton.set_adjustment(adjustment)
		hbox.pack_start(label, True, True, 0)
		hbox.pack_start(self.rest_spinbutton, False, True, 0)
		self.rest_spinbutton.set_update_policy(gtk.SpinButtonUpdatePolicy.IF_VALID)
		self.rest_spinbutton.set_digits(1)
		self.rest_spinbutton.set_value(REST_SECONDS / 60.0)

		listbox.add(row)
	
		row = gtk.ListBoxRow()
		hbox = gtk.Box(orientation=gtk.Orientation.HORIZONTAL, spacing=50)
		row.add(hbox)
		label = gtk.Label("Long rest period duration", xalign=0)
		adjustment = gtk.Adjustment(0, 0, 100, 1, 10, 0)
		self.lrest_spinbutton = gtk.SpinButton()
		self.lrest_spinbutton.set_adjustment(adjustment)
		hbox.pack_start(label, True, True, 0)
		hbox.pack_start(self.lrest_spinbutton, False, True, 0)
		self.lrest_spinbutton.set_update_policy(gtk.SpinButtonUpdatePolicy.IF_VALID)
		self.lrest_spinbutton.set_digits(1)
		self.lrest_spinbutton.set_value(LONG_REST_SECONDS / 60.0)

		listbox.add(row)

		row = gtk.ListBoxRow()
		hbox = gtk.Box(spacing=25)
		row.add(hbox)
		button1 = gtk.Button(label="Cancel")
		button2 = gtk.Button(label="Accept")
		button1.connect("clicked", self.options_window_cancel)
		button2.connect("clicked", self.options_window_accept)
		hbox.pack_start(button1, True, True, 0)
		hbox.pack_start(button2, True, True, 0)

		listbox.add(row)

		self.options_window.show_all()


	def options_window_accept(self, widget):
		global CYCLES_TIL_LONG_REST, WORK_SECONDS, REST_SECONDS, LONG_REST_SECONDS

		CYCLES_TIL_LONG_REST = self.cycles_spinbutton.get_value_as_int()
		WORK_SECONDS = self.work_spinbutton.get_value() * 60
		REST_SECONDS = self.rest_spinbutton.get_value() * 60
		LONG_REST_SECONDS = self.lrest_spinbutton.get_value() * 60

		self.reset('activate')

		save_settings()

		self.options_window.close()

	def options_window_cancel(self, widget):
		self.options_window.close()

	def quit(self, source):
		gtk.main_quit()


	def timer(self):
		if self.status != 'running': return
		if self.seconds > 0:
			self.seconds -= 1
			self.formatted_set_label(self.seconds)
			glib.timeout_add_seconds(1, self.timer)
		else:
			self.period_end_notification()
			self.toggle_state()
			if self.work_state:
				self.seconds = WORK_SECONDS
			else:
				self.cycles += 1
				if self.cycles == CYCLES_TIL_LONG_REST+1:
					self.cycles = 0
					self.seconds = LONG_REST_SECONDS
				else:
					self.seconds = REST_SECONDS
			glib.timeout_add_seconds(1, self.timer)
	

	def toggle_state(self):
		self.work_state = not self.work_state

	def period_end_notification(self):
		if self.work_state:
			call(["notify-send", "Work over", "Rest for a while!"])
			call(['paplay', INSTALL_PATH + '/media/notification_sound.oga'])
		else:
			call(['notify-send', 'Rest over', 'Back to work!'])
			call(['paplay', INSTALL_PATH + '/media/notification_sound.oga'])


if __name__ == "__main__":
	if sys.version_info > (3, 0): #if invoked with python3
		try:
			load_settings()
		except FileNotFoundError:
			# first time starting
			save_settings()

		p = PomodoroTimer()
		gtk.main()
	else:
		try:
			load_settings()
		except IOError:
			# first time starting
			INSTALL_PATH = sys.path[0]
			save_settings()

		p = PomodoroTimer()
		gtk.main()



