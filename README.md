gPomTimer is a Pomodoro Timer for Unity and GNOME 3 desktop environments.

*XFCE is also supported via the Indicator Plugin for xfce-panel.*

This program requires Python 2.7 or 3.4 and GObject Introspection.

On Debian/Ubuntu: 

``` shell
apt-get install gobject-introspection
apt-get install python-gi # or python3-gi
```

Then, just download the repository wherever you'd like and run ind.py.
The default settings will set a work block to be 50 minutes followed by a 10 minute rest. 
After every 3 cycles the rest is extended to 15 minutes.